This is an OpenPGP proof that connects my OpenPGP key to this Gitea account. For details check out https://keyoxide.org/guides/openpgp-proofs

[My Keyoxide profile](https://keyoxide.org/a3c12d45d81875ed6d9b2e25994b4a1066c92871)
